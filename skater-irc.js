#! /usr/bin/env node
'use strict';

const io = require('socket.io-client');
const hue_server_address = 'https://hue.merkoba.com';
const fs = require('fs');
const irc = require('irc');
const path = require('path');
const files_location = './';
const files_path = path.normalize(path.resolve(__dirname, files_location) + '/');
const config_path = `${files_path}config.json`;


/* INIT */

// Start the connection process
let config = get_config();

let irc_interface= start_irc();

let hue_interface = start_hue();

hue_interface.set_handler(irc_interface);
irc_interface.set_handler(hue_interface);
console.info('Connected');


/* CONFIGURATION */

// Reads the config JSON file
function get_config() {
	if(fs.existsSync(config_path)) {
		let config = JSON.parse(fs.readFileSync(config_path, 'utf8'));

		if(!config.email || !config.password) {
			console.info('Check config.json');
			process.exit(1);
			return false;
		}
		return config;
	} 
}

/* IRC PART */

// Starts the IRC client and configures it
function start_irc() {
	let irc_client = new irc.Client(
		config.irc_server,
		config.irc_username, 
		{ channels: [config.irc_channel] }
	);
	return {
		'say': message =>
			irc_client.say(config.irc_channel, message),
		'set_topic': topic =>
			irc_client.send('TOPIC', config.irc_channel, topic),
		'set_handler': hue => {
			irc_client.on(
				'registered',
				()=> {
					console.info('Connected to IRC');

					if(config.irc_identify_password) {
						irc_client.say('NickServ', `IDENTIFY ${config.irc_identify_password}`);
					}
				}
			);

			irc_client.addListener(
				`message${config.irc_channel}`,
				(from, message) => hue.say(`<${from}> ${message}`)
			);

			irc_client.addListener(
				'error',
				message => console.info(`Error: ${message}`)
			);
		}
	};
}



/* HUE PART */

// Starts the Hue socket and configures it
function start_hue() {
	let hue_socket = io.connect(
		hue_server_address, 
		{ reconnection: false }
	);

	// Centralized function to send emits to the Hue server
	function hue_socket_emit(method, data) {
		data.server_method_name = method;
		hue_socket.emit('server_method', data);
	}
	// Removes unwanted formatting from Hue chat messages
	function hue_clean_chat_message(message) {
		return message.replace(/\=?\[dummy\-space\]\=?/gm, '');
	}
	return {
		'say': message => hue_socket_emit('sendchat', {message:message}),
		'set_handler': irc => {
			// Object with all the supported Hue event handlers
			// Each function receives a data object
			let hue_socket_events = {
				chat_message: data => {
					if(data.username !== config.hue_username) {
						let link_title = data.link_title ? ` (${data.link_title})` : '';
						let message = hue_clean_chat_message(data.message);
						let edited = data.just_edited ? ` (edited)` : '';
                        irc.say(`${data.username}: ${message + link_title + edited}`);
					}
				},
				changed_image_source: data => {
					irc.say(
						data.source.startsWith('/')?
						`${data.setter} changed the Image to: https://hue.merkoba.com${data.source}`:
						`${data.setter} changed the Image to: ${data.source}`
					);
						
					if(data.comment) {
						irc.say(`${data.setter}: ${data.comment}`);
					}
				},
				changed_tv_source: data => {
					let title = data.title ? ` (${data.title})` : '';
					irc.say(`${data.setter} changed the TV to: ${data.source + title}`)
						
					if(data.comment) {
						irc.say(`${data.setter}: ${data.comment}`);
					}
				},
				changed_radio_source: data => {
					let title = data.title ? ` (${data.title})` : '';
					irc.say( `${data.setter} tuned the Radio to: ${data.source + title}`);
						
					if(data.comment) {
						irc.say(`${data.setter}: ${data.comment}`);
					}
				},
				topic_change: data =>
					irc.set_topic(`${config.topic_base} | ${data.topic}`),

				user_join: data => {
					if(config.show_joins) {
						irc.say( `${data.username} joined`);
					}
				},
				user_disconnect: data => {
					if(config.show_parts) {
						irc.say(`${data.username} has left`);
					}
				}
			};
				

			hue_socket.on(
				'connect',
				()=> {
					hue_socket_emit(
						'join_room',
						{
							alternative: true,
							room_id: config.roomid,
							email: config.email,
							password: config.password
						}
					);
					console.info('Connected to HUE');
				}
			);

			hue_socket.on(
				'update',
				obj => {
					let action = hue_socket_events[obj.type];
					if(action){
						action(obj.data);
					}
				}
			);

			hue_socket.on(
				'disconnect',
				()=> setTimeout(
					()=> console.log(hue_socket.connect()),
					5000
				)
			);
		}
	};
}



